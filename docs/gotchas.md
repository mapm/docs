# Potential Gotchas

## Booleans

Because of the way the `yaml_rust` crate works, and the (in our opinion) verbose specifications of the YAML language, the values `true`, `false`, `yes`, `no`, `on`, and `off` are parsed as booleans. However, because of the way TeX works, we cast every value into a string.

This means that `yes` and `on` will be incorrectly cast to `true`, and `no` and `off` will be cast to `false`. We currently see no way to avoid this, as the `strict_yaml` crate does not support Serde. We do not expect anyone to use these values anyway, but are including it just for completeness sake.

## LaTeX: Using mapm vars in `\section`

You can't write something like
```
\section{Problem 1 (\probvar{1}{author})}
```
directly, because under the hood, `\probvar` calls `\input`. (mapm does this because it's the only way to make Asymptote work properly.) However, since `\input` is a fragile command, you have to `\protect` it when you're in a section. So correct LaTeX would look like
```
\section{Problem 1 (\protect\probvar{1}{author})}
```
