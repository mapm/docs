# Contests

mapm uses [problem files](/problems) in your problems directory (defined in the file `$CONFIG/profile`) and [templates](/contests/template) in `$CONFIG/templates`.

To create a contest, create a file with a `.yml` extension. To make things easier we call the file `contest.yml`, but know the filename can be anything - it does not matter what it is called.

Contests must have an array with key `problems` that contains the names of each problem, and it must also have a string with key `template` corresponding to a template file. For instance, a very minimal example of a `contest.yml` would be

```yaml
template: minimal-template
vars:
  year: 2021
problems:
  - tally-miscount
  - group-backwards
  - eamc-cyclic
  - angle-bisector-ptolemy
  - max-poly
  - monic-6th-degree
  - phi-fraction
  - tangent-ellipse
  - alexander-balls
  - carpet
  - alien-ages
  - cauchy-telescope
```

It would correspond to the template below.

```yaml
problem_count: 12
engine: pdflatex
texfiles:
  problems.tex: MAT-{year}.pdf
vars:
  - year
problemvars: []
solutionvars: []
```

If your template has variables, they **must** also be present in `contest.yml`. To make things simple, your variables may only take string values.

You are also allowed to specify a `problem_count` in `contest.yml`. This will take priority and override `problem_count` from `template.yml` if it exists.
