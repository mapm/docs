# Templates

A **template** is a directory inside `$CONFIG/templates` that consists of a `config.yml` and any number of `.tex` files.

For instance, if we wanted a template named `mat`, we would have the directory `$CONFIG/templates/mat` and the file `$CONFIG/templates/mat/config.yml`, along with some `.tex` files in the directory

What follows will form an [example template](https://gitlab.com/mathadvance/mapm/mapm/-/blob/release/LICENSE.md).

## config.yml

In `config.yml` the following variables can/need to be defined:

- An `engine`.

  We only officially support `pdflatex`, `xelatex`, and `lualatex`, but under the hood we are just calling `latexmk -pdflatex=ENGINE -pdf problems.tex` so you may have some luck with more exotic engines.

- A `texfiles` with key-val pairs (where values must be strings). Keys are the names of the `.tex` files in the directory, values are the names of the compiled PDF.

  Template variables (for example, `{title}`) can be passed into the value and parsed correctly based on the value passed into `vars` (see below). They are delimited with curly braces, as they have no special meaning in Strict YAML (unlike regular YAML).

- An **optional integer** `problem_count`. This is the number of problems that should be in your contest. This helps `mapm build` report a helpful error if the number of problems is incorrect, rather than letting latexmk fail silently, or having a problem not show up.

	However, for contests with a variable number of problems, this will not be helpful, so you may elect not to include the `problem_count` key at all, or to include it in `contest.yml`.

- A string array `vars`.

  This sets up the allowed variables for [contest files](contests). On one hand, the contest file will warn you if any variables passed into the contest file aren't defined in the problem file. On the other, it will **throw an error** if variables defined in `config.yml` are not defined in the contest file.

- A string array `problemvars`.

  This checks that any problem used in a contest using this template has these variables set. This is so errors can helpfully be caught and displayed by the library, rather than having cryptic error messages pop up when latexmk tries to do its thing.

  Only variables used in the TeX document itself should be passed into this string array. Thus, if you want to track something like difficulty through a variable in your problem files, you can enforce it by using `mapm find` to find any files that do not have the `difficulty` variable set.

- A string array `solutionvars`.

  Serves the same function as `problemvars`, only it's used for the `\solvar` LaTeX macro.

Here is an example of a valid `config.yml`.

```yaml
engine: pdflatex
problem_count: 12
texfiles:
  problems.tex: "{title}.pdf"
  solutions.tex: "{title}-sols.pdf"
vars:
  - title
  - year
```

(We string escape because otherwise, YAML would read the `{` character and interpret it as "start a block".)

## TeX Macros

Don't define these macros in your template, because `mapm.sty` is going to use them.

- \mapmvar
- \probcount
- \probname
- \probvar
- \solvar
- \solcount

And that's it! mapm tries to get out of your way, so it does not reserve a lot of macros for its public interface.

### mapmvar

To access the values of the variables defined in [a contest file](contests) (see the section on `config.yml` in this page of the documentation), use `\mapmvar{key}` to get the value associated with said key. For instance, if `contest.yml` looks like this

```yaml
vars:
  - title: Math Advancement Tournament
  - year: 2021
```

then writing `\mapmvar{year}` in any template `.tex` file will print `2021` for this specific contest.

### probcount

Writing `\probcount` returns the number of problems in your contest.

This is useful for looping through every problem in a contest, particularly if there are a variable number of problems.

### probname

Writing `\probname{n}` returns the problem name (determined by the filename) of problem `n`.

### probvar

Similar to `\mapmvar`, writing `\probvar{n}{var}` gets the value associated with variable `var` in problem `n` of the contest.

Note that problems are 1-indexed.

Take the following `config.yml`

```yaml
problems:
  - addition
```

and let `addition.yml`, which must be in the directory defined by the `MAPM_PROBLEM_PATH` environment variable, have

```yaml
problem: What is $1+1$?
```

Then `\probvar{1}{problem}` will return `What is $1+1$?`

### solvar

`\solvar{n}{i}{var}` will return the value of `var` for solution `i` of problem `n`. Take the following example, building off of the previous one.

`contest.yml`:

```yaml
problems:
  - addition
```

`addition.yml`:

```yaml
problem: What is $1+1$?
solutions:
  - text: It's probably $2$.
    author: Dennis Chen
```

### solcount

To get the number of solutions of problem `n` in a contest, write `\solcount{n}`.

This is only intended to be used to define `\solutions` as seen in the example `solutions.tex` that will follow soon.

## texfiles

Any key passed into `texfiles` must also be a complete `.tex` document inside the tempalte directory, as it will be copy-pasted verbatim into the contest build directory when `mapm build` is run. It also must contain `\usepackage{mapm}`.

These `texfiles` are the only documents that will be compiled with `latexmk`. You can include other files --- like other `.tex` subfiles meant to be included or images --- in the template directory. Do note that the file `mapm-vars.tex` will be generated by `mapm build`, so do not include a file with that name in your template whatsoever.

For simplicity's sake, let us build off of our `config.yml` and use `problems.tex` and `solutions.tex` as examples --- they are the two texfiles that most people will commonly use.

### problems.tex

To input the variable with key `key`, use `\mapmvar{key}` and it will return the value of `key`.

You can also get the name of a certain problem with `\probname{number}`. This is not so useful for actual contests, but can be used for the `preview` and `preview-all` templates.

Here is an example of a valid `problems.tex` based on the previous `config.yml`.

```tex
\documentclass{article}
\usepackage{mapm}

\newcommand{\problem}[1]{%
  \probvar{#1}{problem}% You might want to put this in your own mapm extension package.
}

\title{\mapmvar{title}}
\author{Math Advance}
\date{\mapmvar{year}}

\begin{document}
\maketitle

\problem{1} % equivalent to writing \probvar{1}{problem}
\end{document}
```

### solutions.tex

In this template, to input every solution of problem `n`, we use `\solutions{n}`. (In theory you can define and use this command in `problems.tex`, or indeed, any `texfile`, but it semantically makes zero reason to do so outside of the solutions file. The same applies for `\solvar` in general.)

Here is an example of a valid `solutions.tex`, adapted from the previous `problems.tex`.

```tex
\documentclass{article}
\usepackage{mapm}
\usepackage{pgffor}

% You might want to put these macros in your own mapm extension package.

\newcommand{\problem}[1]{%
  \probvar{#1}{problem}%
}

\newcommand{\solutions}[1]{%
  \foreach \n in {1,...\solcount{#1}} {
    \emph{Solution by \solvar{#1}{\n}{author}}

    \solvar{#1}{\n}{text}
  }
}

\title{\mapmvar{title} Solutions}
\author{Math Advance}
\date{\mapmvar{year}}

\begin{document}
\maketitle

\problem{1}
\solutions{1}

\end{document}
```

## Dotfiles are ignored

Any hidden files or directories (i.e. files or directories that begin with `.`) will *not* be copied. The motivating reason for this change is because of `.git` (tracking templates with version control is good).
