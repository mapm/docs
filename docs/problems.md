# Problems

## Problem Directory

All problems are read from the directory `$CONFIG/problems/PROFILE`, where `PROFILE` represents the active profile. The active profile is determined by the contents of the file `$CONFIG/profile`. For instance, if that file contains the string `mathadvance`, then the active profile is `mathadvance` and mapm will query `$CONFIG/problems/mathadvance` for problems.

You can also use `mapm profile set mathadvance` to set the profile to mathadvance. This will automatically create the problem directory for you if it does not already exist.

## Collaboration

If you are collaborating with other power users, my recommendation is that you use Git and run mapm through the command-line interface. Otherwise, use Dropbox and the mapm GUI.

## Problem Files

To make a problem, create a `.yml` file in `$CONFIG/problems/PROFILE`. The filename will be the problem title. It is strongly recommended that you do not use spaces or another weird characters that may need to be escaped. (If you're collaborating, keep in mind that your filenames also need to be valid on all major OSes.)

As an example, let the title of the problem we are creating be called `basic-addition`. Then create a file `basic-addition.yml` with the following:

```yaml
problem: Find the value of $1+2+3+4$.
author: Dennis Chen
answer: $10$
solutions:
  - text: You do some very hard addition and get $10$.
    author: Dennis Chen
  - text: |-
        This problem is too hard for its position.
        Somehow, you add these numbers and get $10$?
    author: John Doe
```

Then, in your [template file](/contests/template), you can access the problem, author, and answer through `\probvar{n}{problem}`, `\probvar{n}{author}`, and `\probvar{n}{answer}` respectively, where `n` is the problem number.

To access the first solution's text, use `\solvar{n}{1}{text}`, and to access its author, use `\solvar{n}{1}{author}`.

### Recommendations for Multiple Authors

mapm does not make any assumptions on how you would like to format multiple authors. Therefore, it is my recommendation that a problem with multiple authors looks like the following:

```yaml
problem: |-
    Alexander is juggling balls. At the start, he has four balls in his left
    hand: a red ball, a blue ball, a yellow ball, and a green ball. Each
    second, he tosses a ball from one hand to the other. If Alexander juggles
    for $20$ seconds and the number of ways for Alexander to toss the balls
    such that he has all four balls in his right hand at the end is $N$, find
    the number of positive divisors of $N$.
author: Dennis Chen, Aaron Guo
```

It is a bit awkward to have the tag called `author` when there are multiple authors, but there's nothing we can do about it without making the source code of mapm more complex.
