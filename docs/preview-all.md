# Preview-all

This information is pertinent to setitng up the `mapm preview-all` command. Configuring it is pretty much the same as `mapm preview`.

## Contest

Under the hood, the contest with the following yaml is compiled:

```
template: preview-all
vars: {}
problems: [PROBLEMS]...
```

where `[PROBLEMS]...` represents the array of all problems that `preview-all` is compiling.

## Template

The `vars` key of your template must be empty (because the contest has no variables) and the `problem_count` key must also be empty, because you can have a variable number of problems. For instance, you can (and should) enforce some problem and solution variables.

## Script

When running `mapm preview-all`, the script with name `preview-all` is run. By default, it opens your preferred PDF viewer. The exact default command is OS-specific.
