# Home

This the documentation for the mapm command line interface. The same principles apply to the GUI, especially with contests, problems, and templates.

For a quick summary of what mapm is about, head to the [About](/about) page.

All config files use the `yaml` language and the `.yml` extension (you cannot use `.yaml`, we follow the philosophy of convention over configuration).

In the documentation, the `$CONFIG` variable will be referenced frequently. The value of this variable varies based on the operating system, and it is defined by the [dirs](https://docs.rs/dirs/latest/dirs/fn.config_dir.html) crate.

If you are not so experienced with computers, LaTeX, or Rust, you may want to read [From Zero to Mapm](https://gitlab.com/mapm/zero-to-mapm).
