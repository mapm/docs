# About

mapm, which stands for "mapm ain't problem management", is a *contest management software* that stores problems in files so that they can be interchangably swapped around in contests. As such, the library is designed and the command-line interface is implemented to provide first-class support for programmatically compiling contests, though it is possible to tag and search problems.

For a practical demonstration of why such a program is helpful, see my personal website's article, [Why mapm?](https://dennisc.net/writing/tech/why-mapm)
